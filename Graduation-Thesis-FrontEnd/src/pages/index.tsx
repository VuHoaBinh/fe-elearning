export * from "./AuthPage";
export * from "./CoursePage";
export * from "./MainPage";
export * from "./AdminPage";
export * from "./TeacherPage";
export * from "./StudentPage";
export * from "./QuizPage";
